#!/usr/bin/python3.4
"""Generates random peptides from Uniprot sequences"""
import re
import requests

import click

__author__ = "Jean Manguy"
__version__ = "0.1"
__license__ = "MIT"


class RandomPeptides(object):
    def __init__(self, uniprotid, count, length=None, minlength=None, maxlength=None):
        self.uniprotid = uniprotid
        self.header = ''
        self.sequence = ''
        self.count = count
        self.length = length
        self.minlength = minlength
        self.maxlength = maxlength

    def is_uniprot(self):
        uniprotregex = r'[OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}'
        return re.match(uniprotregex, self.uniprotid)

    @staticmethod
    def fasta_to_strings(fasta):
        header = ''
        fastalist = fasta.split('\n')
        if isinstance(fastalist[0], basestring) and len(fastalist[0]) > 0 and fastalist[0][0] == '>':
            header = fastalist.pop(0)  # remove the comment of fasta if exists
        return header, ''.join(fastalist)

    def download_fasta(self):
        headers = {'user-agent': 'my-app/0.0.1'}
        try:
            req = requests.get("http://www.uniprot.org/uniprot/{uniprotid}.fasta".format(uniprotid=self.uniprotid))
        except HTTPError as e:
            click.echo('Failed to retrieve {id} : error {nerr}'.format(id=self.uniprotid, nerr=e.code), err=True)
            raise
        except URLError as e:
            click.echo('Failed to retrieve {id} reason: {reason}'.format(id=self.uniprotid, reason=e.reason), err=True)
            raise
        return r.text

    def get_sequence(self):
        if is_uniprot(self):
            try:
                fasta = self.download_fasta(self.uniprotid)
                header, sequence = self.fasta_to_strings(fasta)
            except Exception:
                raise
        else:
            click.echo('Invalid Uniprot id : {userid}'.format(userid=self.uniprotid), err=True)
            raise Exception
        return header, sequence

# def randompeptides(uniprotid, count, length = None, minlength = None, maxlength = None):
#     try:
#         header, sequence = get_sequence(uniprotid)
#         click.echo(sequence)
#         for c in range(count):
#             pass
#     except Exception:
#         pass
