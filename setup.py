from setuptools import setup

setup(
    name='RandomPeptides',
    py_modules=['ramdompeptides'],
    install_requires=[
        'Click', 'requests'
    ],
    entry_points='''
        [console_scripts]
        randompeptides=cli:cli
    ''',
)
