#!/usr/bin/python3.4
__author__ = "Jean Manguy"

import click

import randompeptides


@click.command()
@click.argument('uniprotid', nargs=-1, type=click.STRING)
@click.option('-c', '--count', 'count', default=100, help='Number of peptides. (min: 0, max: 1000, default: 100)',
              type=click.IntRange(0, 1000, clamp=True))
@click.option('-l', '--length', help='Peptides length. (min: 2, max: 40, default: 2)',
              type=click.IntRange(2, 40, clamp=False))
@click.option('--min-length', 'minlength',
              help='Peptides minimal length. Required if --length is not used. (min: 2, max: 40, default: 2)',
              type=click.IntRange(2, 40, clamp=True))
@click.option('--max-length', 'maxlength',
              help='Peptides maximal length. Required if --length is not used. (min: 2, max: 40, default: 40)',
              type=click.IntRange(2, 40, clamp=True))
def cli(uniprotid, count, length, minlength, maxlength):
    """Generates random peptides from Uniprot sequences"""
    for id in uniprotid:
        if length:
            rp = randompeptides.RandomPeptides(id, count, length=length)
        elif minlength and maxlength:
            rp = randompeptides.RandomPeptides(id, count, minlength=minlength, maxlength=maxlength)
        else:
            click.echo('Please enter values for the length. Use --help to display all the options', err=True)
            break
